package com.capstone.contructor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContructorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContructorApplication.class, args);
    }

}

