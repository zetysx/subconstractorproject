USE [master]
GO
/****** Object:  Database [ContractorProject]    Script Date: 1/12/2019 11:30:18 AM ******/
CREATE DATABASE [ContractorProject]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SubcontractorProject', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\SubcontractorProject.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SubcontractorProject_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\SubcontractorProject_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ContractorProject] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ContractorProject].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ContractorProject] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ContractorProject] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ContractorProject] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ContractorProject] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ContractorProject] SET ARITHABORT OFF 
GO
ALTER DATABASE [ContractorProject] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ContractorProject] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ContractorProject] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ContractorProject] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ContractorProject] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ContractorProject] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ContractorProject] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ContractorProject] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ContractorProject] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ContractorProject] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ContractorProject] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ContractorProject] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ContractorProject] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ContractorProject] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ContractorProject] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ContractorProject] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ContractorProject] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ContractorProject] SET RECOVERY FULL 
GO
ALTER DATABASE [ContractorProject] SET  MULTI_USER 
GO
ALTER DATABASE [ContractorProject] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ContractorProject] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ContractorProject] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ContractorProject] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ContractorProject] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'ContractorProject', N'ON'
GO
USE [ContractorProject]
GO
/****** Object:  Table [dbo].[Docket]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Docket](
	[id] [int] NOT NULL,
	[docketName] [nvarchar](50) NOT NULL,
	[startDate] [date] NOT NULL,
	[endDate] [date] NOT NULL,
	[locatonId] [int] NOT NULL,
	[price] [float] NOT NULL,
	[status] [int] NOT NULL,
	[subcontractorId] [varchar](40) NULL,
	[primecontructorId] [varchar](40) NULL,
 CONSTRAINT [PK_Docket] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocketWorker]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocketWorker](
	[id] [int] NOT NULL,
	[docketId] [int] NOT NULL,
	[workerId] [varchar](40) NOT NULL,
 CONSTRAINT [PK_DocketWorker] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Evidence]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Evidence](
	[id] [int] NOT NULL,
	[picture] [varchar](130) NOT NULL,
	[docketId] [int] NOT NULL,
 CONSTRAINT [PK_Evidence] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Form]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Form](
	[id] [int] NOT NULL,
	[formName] [nvarchar](50) NOT NULL,
	[formDetail] [nvarchar](300) NOT NULL,
	[createdTime] [datetime] NOT NULL,
	[author] [varchar](40) NOT NULL,
	[isTemplate] [bit] NOT NULL,
	[docketId] [int] NOT NULL,
 CONSTRAINT [PK_Form] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Location]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[id] [int] NOT NULL,
	[city] [nvarchar](70) NOT NULL,
	[district] [nvarchar](70) NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notification]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Notification](
	[id] [int] NOT NULL,
	[userTo] [varchar](40) NOT NULL,
	[time] [datetime] NOT NULL,
	[action] [varchar](200) NOT NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PrimeConstructor]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrimeConstructor](
	[username] [varchar](40) NOT NULL,
	[companyName] [nvarchar](60) NOT NULL,
	[businessLicense] [varchar](50) NULL,
	[faxNumber] [varchar](20) NULL,
 CONSTRAINT [PK_PrimeConstructor] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Profession]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profession](
	[professionId] [int] NOT NULL,
	[professionName] [nvarchar](60) NOT NULL,
 CONSTRAINT [PK_Profession] PRIMARY KEY CLUSTERED 
(
	[professionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Request]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Request](
	[id] [int] NOT NULL,
	[primecontructorId] [varchar](40) NOT NULL,
	[subcontructorId] [varchar](40) NOT NULL,
	[startDate] [date] NOT NULL,
	[endDate] [date] NOT NULL,
	[locationId] [int] NOT NULL,
	[workerNumber] [int] NOT NULL,
	[price] [float] NOT NULL,
	[professionId] [int] NOT NULL,
	[isConfirm] [bit] NOT NULL,
	[confirmDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Request] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[roleId] [int] NOT NULL,
	[roleName] [varchar](30) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScheduleBusyWorker]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScheduleBusyWorker](
	[id] [int] NOT NULL,
	[username] [varchar](40) NOT NULL,
	[date] [date] NOT NULL,
 CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subcontractor]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subcontractor](
	[username] [varchar](40) NOT NULL,
	[companyName] [nvarchar](50) NOT NULL,
	[faxNumber] [varchar](20) NULL,
	[description] [nvarchar](120) NULL,
	[availableWorker] [int] NOT NULL,
 CONSTRAINT [PK_Subcontractor] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubcontractorLocation]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubcontractorLocation](
	[id] [int] NOT NULL,
	[locationId] [int] NOT NULL,
	[subcontractorId] [varchar](40) NOT NULL,
 CONSTRAINT [PK_SubcontractorLocation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubcontractorProfession]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubcontractorProfession](
	[id] [int] NOT NULL,
	[professionId] [int] NOT NULL,
	[subcontractorId] [varchar](40) NOT NULL,
	[price] [float] NOT NULL,
 CONSTRAINT [PK_SubcontractorProfession] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[username] [varchar](40) NOT NULL,
	[password] [varchar](40) NOT NULL,
	[phone] [varchar](12) NOT NULL,
	[address] [nvarchar](120) NOT NULL,
	[firstName] [nvarchar](60) NULL,
	[lastName] [nvarchar](60) NULL,
	[avatar] [varchar](120) NULL,
	[email] [varchar](100) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserRole](
	[id] [int] NOT NULL,
	[username] [varchar](40) NOT NULL,
	[roleId] [int] NOT NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Worker]    Script Date: 1/12/2019 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Worker](
	[username] [varchar](40) NOT NULL,
	[subcontractorId] [varchar](40) NOT NULL,
 CONSTRAINT [PK_Worker] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Docket]  WITH CHECK ADD  CONSTRAINT [FK_Docket_PrimeConstructor] FOREIGN KEY([primecontructorId])
REFERENCES [dbo].[PrimeConstructor] ([username])
GO
ALTER TABLE [dbo].[Docket] CHECK CONSTRAINT [FK_Docket_PrimeConstructor]
GO
ALTER TABLE [dbo].[Docket]  WITH CHECK ADD  CONSTRAINT [FK_Docket_Subcontractor] FOREIGN KEY([subcontractorId])
REFERENCES [dbo].[Subcontractor] ([username])
GO
ALTER TABLE [dbo].[Docket] CHECK CONSTRAINT [FK_Docket_Subcontractor]
GO
ALTER TABLE [dbo].[DocketWorker]  WITH CHECK ADD  CONSTRAINT [FK_DocketWorker_Docket] FOREIGN KEY([docketId])
REFERENCES [dbo].[Docket] ([id])
GO
ALTER TABLE [dbo].[DocketWorker] CHECK CONSTRAINT [FK_DocketWorker_Docket]
GO
ALTER TABLE [dbo].[DocketWorker]  WITH CHECK ADD  CONSTRAINT [FK_DocketWorker_Worker] FOREIGN KEY([workerId])
REFERENCES [dbo].[Worker] ([username])
GO
ALTER TABLE [dbo].[DocketWorker] CHECK CONSTRAINT [FK_DocketWorker_Worker]
GO
ALTER TABLE [dbo].[Evidence]  WITH CHECK ADD  CONSTRAINT [FK_Evidence_Docket] FOREIGN KEY([docketId])
REFERENCES [dbo].[Docket] ([id])
GO
ALTER TABLE [dbo].[Evidence] CHECK CONSTRAINT [FK_Evidence_Docket]
GO
ALTER TABLE [dbo].[Form]  WITH CHECK ADD  CONSTRAINT [FK_Form_Docket] FOREIGN KEY([docketId])
REFERENCES [dbo].[Docket] ([id])
GO
ALTER TABLE [dbo].[Form] CHECK CONSTRAINT [FK_Form_Docket]
GO
ALTER TABLE [dbo].[Form]  WITH CHECK ADD  CONSTRAINT [FK_Form_User] FOREIGN KEY([author])
REFERENCES [dbo].[User] ([username])
GO
ALTER TABLE [dbo].[Form] CHECK CONSTRAINT [FK_Form_User]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_User1] FOREIGN KEY([userTo])
REFERENCES [dbo].[User] ([username])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_User1]
GO
ALTER TABLE [dbo].[PrimeConstructor]  WITH CHECK ADD  CONSTRAINT [FK_PrimeConstructor_User] FOREIGN KEY([username])
REFERENCES [dbo].[User] ([username])
GO
ALTER TABLE [dbo].[PrimeConstructor] CHECK CONSTRAINT [FK_PrimeConstructor_User]
GO
ALTER TABLE [dbo].[Request]  WITH CHECK ADD  CONSTRAINT [FK_Request_Location] FOREIGN KEY([locationId])
REFERENCES [dbo].[Location] ([id])
GO
ALTER TABLE [dbo].[Request] CHECK CONSTRAINT [FK_Request_Location]
GO
ALTER TABLE [dbo].[Request]  WITH CHECK ADD  CONSTRAINT [FK_Request_PrimeConstructor] FOREIGN KEY([primecontructorId])
REFERENCES [dbo].[PrimeConstructor] ([username])
GO
ALTER TABLE [dbo].[Request] CHECK CONSTRAINT [FK_Request_PrimeConstructor]
GO
ALTER TABLE [dbo].[Request]  WITH CHECK ADD  CONSTRAINT [FK_Request_Profession] FOREIGN KEY([professionId])
REFERENCES [dbo].[Profession] ([professionId])
GO
ALTER TABLE [dbo].[Request] CHECK CONSTRAINT [FK_Request_Profession]
GO
ALTER TABLE [dbo].[Request]  WITH CHECK ADD  CONSTRAINT [FK_Request_Subcontractor] FOREIGN KEY([subcontructorId])
REFERENCES [dbo].[Subcontractor] ([username])
GO
ALTER TABLE [dbo].[Request] CHECK CONSTRAINT [FK_Request_Subcontractor]
GO
ALTER TABLE [dbo].[ScheduleBusyWorker]  WITH CHECK ADD  CONSTRAINT [FK_ScheduleBusyWorker_Worker] FOREIGN KEY([username])
REFERENCES [dbo].[Worker] ([username])
GO
ALTER TABLE [dbo].[ScheduleBusyWorker] CHECK CONSTRAINT [FK_ScheduleBusyWorker_Worker]
GO
ALTER TABLE [dbo].[Subcontractor]  WITH CHECK ADD  CONSTRAINT [FK_Subcontractor_User] FOREIGN KEY([username])
REFERENCES [dbo].[User] ([username])
GO
ALTER TABLE [dbo].[Subcontractor] CHECK CONSTRAINT [FK_Subcontractor_User]
GO
ALTER TABLE [dbo].[SubcontractorLocation]  WITH CHECK ADD  CONSTRAINT [FK_SubcontractorLocation_Location] FOREIGN KEY([locationId])
REFERENCES [dbo].[Location] ([id])
GO
ALTER TABLE [dbo].[SubcontractorLocation] CHECK CONSTRAINT [FK_SubcontractorLocation_Location]
GO
ALTER TABLE [dbo].[SubcontractorLocation]  WITH CHECK ADD  CONSTRAINT [FK_SubcontractorLocation_Subcontractor] FOREIGN KEY([subcontractorId])
REFERENCES [dbo].[Subcontractor] ([username])
GO
ALTER TABLE [dbo].[SubcontractorLocation] CHECK CONSTRAINT [FK_SubcontractorLocation_Subcontractor]
GO
ALTER TABLE [dbo].[SubcontractorProfession]  WITH CHECK ADD  CONSTRAINT [FK_SubcontractorProfession_Profession] FOREIGN KEY([professionId])
REFERENCES [dbo].[Profession] ([professionId])
GO
ALTER TABLE [dbo].[SubcontractorProfession] CHECK CONSTRAINT [FK_SubcontractorProfession_Profession]
GO
ALTER TABLE [dbo].[SubcontractorProfession]  WITH CHECK ADD  CONSTRAINT [FK_SubcontractorProfession_Subcontractor] FOREIGN KEY([subcontractorId])
REFERENCES [dbo].[Subcontractor] ([username])
GO
ALTER TABLE [dbo].[SubcontractorProfession] CHECK CONSTRAINT [FK_SubcontractorProfession_Subcontractor]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([roleId])
REFERENCES [dbo].[Role] ([roleId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User] FOREIGN KEY([username])
REFERENCES [dbo].[User] ([username])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO
ALTER TABLE [dbo].[Worker]  WITH CHECK ADD  CONSTRAINT [FK_Worker_Subcontractor] FOREIGN KEY([subcontractorId])
REFERENCES [dbo].[Subcontractor] ([username])
GO
ALTER TABLE [dbo].[Worker] CHECK CONSTRAINT [FK_Worker_Subcontractor]
GO
ALTER TABLE [dbo].[Worker]  WITH CHECK ADD  CONSTRAINT [FK_Worker_User] FOREIGN KEY([username])
REFERENCES [dbo].[User] ([username])
GO
ALTER TABLE [dbo].[Worker] CHECK CONSTRAINT [FK_Worker_User]
GO
USE [master]
GO
ALTER DATABASE [ContractorProject] SET  READ_WRITE 
GO
